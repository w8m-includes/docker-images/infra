FROM alpine:3.17

ARG TERRAFORM_VERSION=1.4.6
ARG KUBECTL_VERSION=v1.27.2
ARG HELM_VERSION=v3.12.0

WORKDIR /usr/local/bin/

RUN wget -O kubectl https://dl.k8s.io/release/${KUBECTL_VERSION}/bin/linux/amd64/kubectl \
&& chmod +x kubectl

RUN wget -O helm.tar.gz https://get.helm.sh/helm-${HELM_VERSION}-linux-amd64.tar.gz  \
&& tar -xvf helm.tar.gz linux-amd64/helm && mv linux-amd64/helm ./helm && rm -r linux-amd64 && rm helm.tar.gz

RUN wget -O terraform.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip \
&& unzip terraform.zip && rm terraform.zip

CMD ["helm", "version"]
