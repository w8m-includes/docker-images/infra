# docker image with terraform, kubectl, helm:

    FROM alpine:3.18.0
    TERRAFORM_VERSION=1.4.6
    KUBECTL_VERSION=v1.27.2
    HELM_VERSION=v3.12.0
